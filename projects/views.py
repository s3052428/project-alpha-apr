from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list_projects.html"

    # In order to list projects which belong to the logged in user only
    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create_project.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        project = form.save()  # save form
        return redirect("show_project", pk=project.pk)
