### Feature 2 ###

- [x] Create a Django project named tracker so that the manage.py file is in the top directory
- [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
- [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
- [x] Create a Django app named tasks and install it in the tracker Django  project in the INSTALLED_APPS list
- [x] Run the migrations
- [x] Create a super user

### Feature 3 ###

- [x] The Project model should have the following attributes.
- [x] The members field shoudl refer to the auth.User model, related name "projects"
Impletemented the user model directly 

### Feature 4 ###

- [x] Register the Project model with the admin so that you can see it in the Django admin site.

### Feature 5 ###

- [x] Create a view that will get all of the instances of the Project model and puts them in the context for the template
- [x] Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
- [x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
- [x] Create a template for the list view that complies with the following specifications


### Feature 6 ###

- [x] In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".


## Notes ##

My apologies for not completing the todo list, given the task requirements on the project webpage, I found it extremely difficult and time consuming to copy/paste the same requirements into the todo list.

In addition, I do take my personal notes in MS One Note which I'm very much used to refering to/taking notes to.

