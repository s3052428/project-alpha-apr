from django.shortcuts import redirect, render
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

# Create your views here.


def usersignup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            # username/pw is extracted from the signup page(form)
            username = form.cleaned_data["username"]
            raw_password = make_password(form.cleaned_data.get("password"))
            # Store Encrypted password in db
            # user is created
            user = User.objects.create_user(username, raw_password)
            login(
                request,
                user,
                backend="django.contrib.auth.backends.ModelBackend",
            )  # login user after it's saved
            return redirect("home")
    # If request is not POST, then create an empty from and redirect to login
    else:
        form = UserCreationForm()

    context = {"form": form}
    return render(request, "registration/signup.html", context)
